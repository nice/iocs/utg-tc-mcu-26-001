#!/usr/bin/env iocsh.bash


require(tc_mcu_26)

# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
epicsEnvSet("ASYN_PORT",      "MCU026_PLC")
epicsEnvSet("IP_ADDR",        "172.30.244.118")
epicsEnvSet("PORT_ADDR",      "5000")
epicsEnvSet("PREFIX",         "UTG-MCU-026")
epicsEnvSet("STREAM_PROTOCOL_PATH", $(tc_mcu_26_DIR)db)

# Connection
drvAsynIPPortConfigure("$(ASYN_PORT)","$(IP_ADDR):$(PORT_ADDR)",0,0,0)
# Loading database
dbLoadRecords(tc_mcu_26.db, "P=$(PREFIX), PORT=$(ASYN_PORT), ADDR=01")

iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# Start the IOC
iocInit()

